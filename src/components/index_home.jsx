import React from 'react'
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import boxesImg from '../assets/img/boxes.png';

const indexHome = () => {
    return (
        <Box
            style={{
                backgroundImage: `url(${boxesImg})`,
                backgroundPosition: 'center center',
                backgroundSize: 'cover',
                height: '50vh',
                backgroundColor: 'black'
            }}
        >
            <Grid 
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                sx={{
                    height: '100%'
                }}
            >
                <Grid item xs={12} sm={10} md={8}>
                    <Typography variant="h2" sx={{
                        color: 'white',
                        textAlgin: 'center'

                    }}>
                        Welcome to Inventory Manager. Your solution to your inventory.
                    </Typography>
                </Grid>
            </Grid>
        </Box>
    )
}

export default indexHome
