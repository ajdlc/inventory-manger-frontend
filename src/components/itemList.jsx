import React from 'react'

const ItemList = (props) => {
    return (
        <div>
            <ul>
                <li>{props.items[0].item.name}</li>
            </ul>
        </div>
    )
}

export default ItemList
