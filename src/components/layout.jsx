import React, { useEffect } from 'react'
import { Outlet } from 'react-router-dom';
import Appbar from "../components/appbar";

const Layout = (props) => {
    const authToken = props.authToken;

    const determineNavLinks = () => {
        if (props.authToken !== "") {
            return [
                {
                  name: "Home",
                  path: "/"
                },
                {
                    name: "Dashboard",
                    path: "/dashboard"
                },
                {
                  name: "Test",
                  path: "/test"
                }
              ]
        } else {
            return [
                {
                  name: "Home",
                  path: "/"
                },
                {
                  name: "Login",
                  path: "/login"
                },
                {
                  name: "Sign Up",
                  path: "/signup"
                },
                {
                  name: "Test",
                  path: "/test"
                }
              ]
        }
    };

    useEffect(() => {
        async function checkRefreshToken() {
            // If there is not an authToken, check to see if we can get a new one using the refresh path
            if (authToken === "") {
                let res2 = await fetch('http://localhost:5002/api/employees/refresh', {
                    credentials: 'include',
                    method: 'POST'
                });
                console.log(res2);
                let data = await res2.json();
                props.onAuthTokenChange(data.token);
            } 
        }
        checkRefreshToken();
    })

    return (
        <div>
            <Appbar items={determineNavLinks()} authToken={props.authToken} onAuthTokenChange={ props.onAuthTokenChange }></Appbar>

            <Outlet></Outlet>
        </div>
    )
}

export default Layout
