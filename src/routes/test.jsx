import React, { useState } from 'react'

export const Test = () => {
    const [count, setCount] = useState(0);
    return (
        <div>
            <input type="number" name="count" id="count" value={count} />
            <div>
                <button onClick={() => {setCount(count +1)}}>Increase Count</button>
            </div>
        </div>
    )
}

export default Test
