import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";

// Components
import ItemList from "../components/itemList";

const Dashboard = (props) => {
    let navigate = useNavigate();
    const authToken = props.authToken;
    const [items, setItems] = useState([]);

    useEffect(() => {
        // Define out async function to get the items for the user
        async function getItems() {
            let url = "http://localhost:5002/api/inventory";
            let data = await fetch(url, {
                headers: {
                    "Authorization": `Bearer ${authToken}`
                }
            });

            let jData = await data.json();
            console.log(jData);
            let jItems = jData.items;
            setItems(jItems);
        }
        // Send the user back to where they came from
        // TODO - Implement a better option here
        if (authToken === "") {
            navigate("/");
        } else {
            console.log(items.length)
            if (items.length === 0) {
                getItems();
            }
        }
    })

    return (
        <div>
            <h2>The dashboard</h2>
            <ItemList items={items}></ItemList>
        </div>
    )
}

export default Dashboard
