import React from 'react';

export default class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log(this.state.email);
        console.log(this.state.password);
    }

    handleEmailChange(event) {
        this.setState({email: event.target.value});
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    render() {
        return (
            <div>
                <main style={{ padding: "1rem 0" }}>
                    <h2>Sign Up</h2>
                    <form action="" style={{
                        margin: "2rem"
                    }}
                    onSubmit={this.handleSubmit}
                    >
                        <label htmlFor="email">Email:</label>
                        <div>
                            <input type="email" name="email" id="email" value={this.state.email} onChange={this.handleEmailChange} autoComplete=""/>
                        </div>

                        <label htmlFor="password">Password:</label>
                        <div>
                            <input type="password" name="password" id="password" onChange={this.handlePasswordChange} value={this.state.password}  />
                        </div>

                        <div>
                            <input type="submit" value="submit" />
                        </div>
                    </form>
                </main>
            </div>
        );
    }
}

// export default signup
