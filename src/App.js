import React, { useState } from 'react';
import { Routes, Route } from "react-router-dom";

// Components
import Layout from "./components/layout"
import IndexHome from "./components/index_home";

// Routes
import Login from "./routes/login";
import Signup from "./routes/signup";
import Test from "./routes/test";
import Dashboard from "./routes/dashboard";

function App() {
  const [authToken, setAuthToken] = useState("");

  return (
    <div>
      <Routes>
        <Route path="/" element={ <Layout authToken={authToken} onAuthTokenChange={ setAuthToken }></Layout> }>
          <Route index element={ <IndexHome></IndexHome> }></Route>
          <Route path="login" element={<Login authToken={authToken} onAuthTokenChange={ setAuthToken }></Login>}></Route>
          <Route path="signup" element={<Signup></Signup>}></Route>
          <Route path="test" element={<Test></Test>}></Route>
          <Route path="dashboard" element={<Dashboard authToken={authToken}></Dashboard>}></Route>
          <Route
            path="*"
            element={
              <main style={{ padding: "1rem" }}>
                <p>There's nothing here!</p>
              </main>
            }
          ></Route>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
